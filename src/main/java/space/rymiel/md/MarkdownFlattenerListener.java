package space.rymiel.md;

import net.kyori.adventure.text.flattener.FlattenerListener;
import net.kyori.adventure.text.format.Style;
import net.kyori.adventure.text.format.TextDecoration;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class MarkdownFlattenerListener implements FlattenerListener {
  private final LinkedList<EnumSet<TextDecoration>> stateStack = new LinkedList<>();
  private final StringBuilder sb = new StringBuilder();
  private final boolean escape;

  private static final Map<TextDecoration, String> DELIMITERS;
  private static final List<TextDecoration> VALID_DECORATIONS;
  private static final List<TextDecoration> VALID_DECORATIONS_REVERSED;

  static {
    final Map<TextDecoration, String> delimiters = new LinkedHashMap<>();

    delimiters.put(TextDecoration.BOLD, "**");
    delimiters.put(TextDecoration.ITALIC, "*");
    delimiters.put(TextDecoration.UNDERLINED, "__");
    delimiters.put(TextDecoration.STRIKETHROUGH, "~~");

    DELIMITERS = Collections.unmodifiableMap(delimiters);
    VALID_DECORATIONS = List.copyOf(delimiters.keySet());
    var reversedList = new ArrayList<>(delimiters.keySet());
    Collections.reverse(reversedList);
    VALID_DECORATIONS_REVERSED = List.copyOf(reversedList);
  }

  MarkdownFlattenerListener(boolean escape) {
    this.escape = escape;
  }

  @Contract(" -> new")
  private static @NotNull EnumSet<TextDecoration> emptyState() {
    return EnumSet.noneOf(TextDecoration.class);
  }

  /** Add the opening delimiters of all decorations set in {@code decorations}. */
  private void openAll(EnumSet<TextDecoration> decorations) {
    for (final var i : VALID_DECORATIONS) {
      if (decorations.contains(i)) {
        sb.append(DELIMITERS.get(i));
      }
    }
  }

  /**
   * Add the closing delimiters of all decorations set in {@code decorations}. Note that this must
   * occur in exact reverse order compared to {@link #openAll}.
   */
  private void closeAll(EnumSet<TextDecoration> decorations) {
    for (final var i : VALID_DECORATIONS_REVERSED) {
      if (decorations.contains(i)) {
        sb.append(DELIMITERS.get(i));
      }
    }
  }

  private EnumSet<TextDecoration> currentState() {
    return Objects.requireNonNullElse(stateStack.peekLast(), emptyState());
  }

  private EnumSet<TextDecoration> popState() {
    return Objects.requireNonNullElse(stateStack.pollLast(), emptyState());
  }

  @Override
  public void pushStyle(@NotNull Style style) {
    var newState = EnumSet.copyOf(currentState());
    var mustOpen = emptyState();
    var mustClose = emptyState();
    for (final var i : VALID_DECORATIONS) {
      var tristate = style.decoration(i);
      if (tristate == TextDecoration.State.TRUE && !currentState().contains(i)) {
        mustOpen.add(i);
        newState.add(i);
      } else if (tristate == TextDecoration.State.FALSE && currentState().contains(i)) {
        mustClose.add(i);
        newState.remove(i);
      }
    }

    stateStack.add(newState);
    closeAll(mustClose);
    openAll(mustOpen);
  }

  @Override
  public void component(@NotNull String text) {
    if (escape) {
      sb.append(
          text.replace("\\", "\\\\")
              .replace("*", "\\*")
              .replace("~", "\\~")
              .replace("_", "\\_")
              .replace("`", "\\`")
              .replace("|", "\\|"));
    } else {
      sb.append(text);
    }
  }

  @Override
  public void popStyle(@NotNull Style style) {
    var newState = EnumSet.copyOf(popState());
    var mustOpen = emptyState();
    var mustClose = emptyState();

    for (final var i : VALID_DECORATIONS) {
      var tristate = style.decoration(i);
      if (tristate == TextDecoration.State.TRUE && !currentState().contains(i)) {
        mustClose.add(i);
        newState.remove(i);
      } else if (tristate == TextDecoration.State.FALSE && currentState().contains(i)) {
        mustOpen.add(i);
        newState.add(i);
      }
    }

    closeAll(mustClose);
    openAll(mustOpen);
  }

  @Override
  public String toString() {
    return sb.toString();
  }
}

package space.rymiel.md;

import net.kyori.adventure.builder.AbstractBuilder;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.flattener.ComponentFlattener;
import net.kyori.adventure.text.serializer.ComponentSerializer;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

public class MarkdownSerializer implements ComponentSerializer<Component, Component, String> {
  private final ComponentFlattener flattener;
  private final boolean escape;

  @Contract(pure = true)
  MarkdownSerializer(ComponentFlattener flattener, boolean escape) {
    this.flattener = flattener;
    this.escape = escape;
  }

  @Override
  public @NotNull Component deserialize(@NotNull String input) {
    throw new UnsupportedOperationException("MarkdownSerializer does not support deserialization");
  }

  @Override
  public @NotNull String serialize(@NotNull Component component) {
    final var state = new MarkdownFlattenerListener(escape);
    flattener.flatten(component, state);
    return state.toString();
  }

  /**
   * A serializer capable of serializing {@link Component}s into simple Markdown format.
   * <b>Deserialization is NOT SUPPORTED</b>.
   *
   * <p>Note that Markdown-like text inside components will be used as-in. If these should be
   * escaped, use {@link #markdownEscaping()} instead!
   *
   * @return a Markdown serializer with no escaping
   */
  @Contract(" -> new")
  public static @NotNull MarkdownSerializer markdownSerializer() {
    return markdown().build();
  }

  /**
   * A serializer capable of serializing {@link Component}s into simple Markdown format.
   * <b>Deserialization is NOT SUPPORTED</b>.
   *
   * <p>Markdown-like text inside components will be escaped with backslashes. This makes it safe
   * for user-input which should <i>not</i> be formatted.
   *
   * @return a Markdown serializer with escaping
   */
  @Contract(" -> new")
  public static @NotNull MarkdownSerializer markdownEscaping() {
    return markdown().escapeMarkdown().build();
  }

  /** Create a new builder for creating a MarkdownSerializer */
  @Contract(" -> new")
  public static @NotNull Builder markdown() {
    return new Builder();
  }

  public static final class Builder implements AbstractBuilder<MarkdownSerializer> {
    private boolean escape = false;
    private ComponentFlattener flattener = ComponentFlattener.basic();

    Builder() {}

    /** Enable escaping of Markdown-like text inside of components */
    @Contract(value = " -> this", mutates = "this")
    public Builder escapeMarkdown() {
      escape = true;
      return this;
    }

    /** Optionally enable escaping of Markdown-like text inside of components */
    @Contract(value = "_ -> this", mutates = "this")
    public Builder escapeMarkdown(boolean enable) {
      escape = enable;
      return this;
    }

    /** Provide a custom {@link ComponentFlattener} to be used by the serializer */
    @Contract(value = "_ -> this", mutates = "this")
    public Builder flattener(ComponentFlattener flattener) {
      this.flattener = flattener;
      return this;
    }

    @Contract(value = " -> new", pure = true)
    @Override
    public @NotNull MarkdownSerializer build() {
      return new MarkdownSerializer(flattener, escape);
    }
  }
}

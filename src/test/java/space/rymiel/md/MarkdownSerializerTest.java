package space.rymiel.md;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.text.format.Style;
import org.junit.jupiter.api.Test;

import static net.kyori.adventure.text.format.TextDecoration.BOLD;
import static net.kyori.adventure.text.format.TextDecoration.ITALIC;
import static net.kyori.adventure.text.format.TextDecoration.STRIKETHROUGH;
import static net.kyori.adventure.text.format.TextDecoration.UNDERLINED;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static space.rymiel.md.MarkdownSerializer.markdownEscaping;
import static space.rymiel.md.MarkdownSerializer.markdownSerializer;

class MarkdownSerializerTest {

  @Test
  void testSimple() {
    var component =
        Component.text()
            .append(Component.text("hello "))
            .append(Component.text("world", Style.style(BOLD)))
            .build();

    assertEquals("hello **world**", markdownSerializer().serialize(component));
  }

  @Test
  void testLonger() {
    final var component =
        Component.text()
            .content("")
            .decoration(BOLD, true)
            .append(
                Component.text()
                    .content("")
                    .color(NamedTextColor.YELLOW)
                    .append(
                        Component.text()
                            .content("Hello ")
                            .append(
                                Component.text()
                                    .content("world")
                                    .decoration(ITALIC, true)
                                    .color(NamedTextColor.GREEN)
                                    .build())
                            .append(Component.text("!"))
                            .build())
                    .build())
            .build();

    assertEquals("**Hello *world*!**", markdownSerializer().serialize(component));
  }

  @Test
  void testRedundant() {
    final var component =
        Component.text()
            .content("Deleted!")
            .decoration(STRIKETHROUGH, true)
            .append(Component.space())
            .append(
                Component.text()
                    .content("Still deleted...")
                    .decoration(STRIKETHROUGH, true)
                    .build())
            .build();

    assertEquals("~~Deleted! Still deleted...~~", markdownSerializer().serialize(component));
  }

  @Test
  void testFalseState() {
    final var component =
        Component.text()
            .content("Bold! ")
            .decoration(BOLD, true)
            .append(
                Component.text()
                    .content("and underline!")
                    .decoration(BOLD, true)
                    .decoration(UNDERLINED, true)
                    .build())
            .append(Component.space())
            .append(Component.text().content("Nothing! ").decoration(BOLD, false).build())
            .append(Component.text("Child of first bold!"))
            .build();

    assertEquals(
        "**Bold! __and underline!__ **Nothing! **Child of first bold!**",
        markdownSerializer().serialize(component));
  }

  @Test
  void testEscape() {
    final var component =
        Component.text().content("Malicious *payload*!").decoration(STRIKETHROUGH, true).build();

    assertEquals("~~Malicious \\*payload\\*!~~", markdownEscaping().serialize(component));
  }

  @Test
  void testEscapeEscape() {
    final var component =
        Component.text()
            .content("Malicious \\*payload\\*!")
            .decoration(STRIKETHROUGH, true)
            .build();

    assertEquals("~~Malicious \\\\\\*payload\\\\\\*!~~", markdownEscaping().serialize(component));
  }

  @Test
  void testNonEscape() {
    final var component =
        Component.text().content("Code -> `look at me!`").decoration(UNDERLINED, true).build();

    assertEquals("__Code -> `look at me!`__", markdownSerializer().serialize(component));
  }
}

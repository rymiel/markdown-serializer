# Markdown Serializer

A very simple serializer to convert [Adventure](https://github.com/KyoriPowered/adventure) Components to a Markdown format.  
**Note that deserialization is NOT yet supported**.

## Dependency

```kotlin
repositories {
  mavenCentral()
  maven("https://gitlab.com/api/v4/projects/37351896/packages/maven")
}

dependencies {
  implementation("space.rymiel:markdown-serializer:1.0.2-SNAPSHOT")
}
```

## Usage

```java
import space.rymiel.md.MarkdownSerializer;

MarkdownSerializer.markdownSerializer().serialize(component);
// Or alternatively:
MarkdownSerializer.markdownEscaping().serialize(component);
```
